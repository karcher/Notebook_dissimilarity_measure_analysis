# This function checks whether the sample order in the otu_table (or distance matrix) and label file are the same or not.
# If the samples are identical and in the same order, simply return label
# If they are not, first check whether matrix and label contain the same samples. If yes, sort label and return sorted label
# Expects label file to be a 1-column data frame with the samples as rownames.

order_label <- function(matrix, label) {
  if (class(matrix) == 'dist'){
    matrix <- as.matrix(matrix)
    message('Converted distance matrix to matrix.')
  }
  if (all(colnames(matrix) == rownames(label))) {
    message('Samples are the same and in order, returning label.')
    return(label)
  } else {
  	message("Checking if label and matrix contain the same samples.")
    samples.label.sorted <- sort(rownames(label))
    samples.matrix.sorted <- sort(colnames(matrix))
	  stopifnot(samples.label.sorted == samples.matrix.sorted)
    message('Samples are the same but unordered, ordering label file...')
    sort.idx <- match(colnames(matrix), rownames(label))
    # resort label and check again
    rnames.label <- rownames(label)
    cnames.label <- colnames(label)
    # For a weird reason, resorting a 1-column df in R inevitably results in a vector (more exactly factor) type conversion of the object.
    label.ordered <- as.data.frame(label[sort.idx,])
    rownames(label.ordered) <- rnames.label[sort.idx]
    colnames(label.ordered) <- cnames.label
    stopifnot(rownames(label.ordered) == colnames(matrix))
    message('returning ordered label file')
    return(label.ordered)
  }
}

# Function to compute JSD distance. Supplied by Paul.
dist.JSD_c <- function(inMatrix, pseudocount = 0.000001, workingDirectory, ...) {
   matrixColSize <- length(colnames(inMatrix))
   matrixRowSize <- length(rownames(inMatrix))
   colnames <- colnames(inMatrix)
   resultsMatrix <- matrix(0, matrixColSize, matrixColSize)
   dyn.load(paste(workingDirectory,'jsd.so',sep='/'))
   r = .C('jsd_dist',as.double(as.matrix(inMatrix)),as.integer(dim(inMatrix)),as.double(pseudocount),as.double(resultsMatrix))
   resultsMatrix <- t(matrix(r[[4]],matrixColSize,matrixColSize))
   colnames -> colnames(resultsMatrix) -> rownames(resultsMatrix)
   as.dist(resultsMatrix)->resultsMatrix
   attr(resultsMatrix, "method") <- "dist"
   return(resultsMatrix)
}

# low-abundance filtering of taxa
filter_features <- function(otutable, filt_method, filt_thresh){
  otu_table <- otutable
  filter_feat <- filt_method
  threshhold <- filt_thresh
  if (filter_feat == "relabundance"){
    # The maximum relative abundance over all samples is used as 
    # filtering method for features regardless of normalization method.
    # Since some data sets are supplied already tss-normalized:
    if (all(as.numeric(apply(otu_table, 2, sum)) <= 1.001)){ # <= instead of = since a potential unassigned fraction could have been removed, resulting in a sum < 1.
      message("not computing tss for feature filtering since already tss.")
      otu_table_tss <- otu_table
    } else if (all(as.numeric(apply(otu_table, 2, sum)) >= 1)){
      message("Computing tss in order to filter features")
      otu_table_tss <- apply(otu_table, 2, function(x) x / sum(x))
    } else {
      warning("Something with your data set is wrong. exiting")
      message("printing apply(otu_table, 2, sum)")
      message(as.numeric(apply(otu_table, 2, sum)))
      q()
    }
    
    keep.idx <- which(apply(otu_table_tss, 1, max) >= as.numeric(threshhold))
    
    # Keep clean
    remove(otu_table_tss)
    otu_table <- otu_table[keep.idx,]
    
    # Sanity check
    stopifnot(all(apply(otu_table, 1, max) >= as.numeric(threshhold)))
  } else if (filter_feat == "none"){
    print("No feature filtering")
  } else {
    write("Feature filtering method unknown; exiting", stderr())
    q()
  }
  return(otu_table)
}

# Function to determine indices of k nearest neighbors from distance vector.
# Note that the distance to the sample of interest to itself has to be removed prior to computation.
k.nearest.neighbors <- function(distance.vector, k = 1){
  stopifnot(!is.null(distance.vector))
  # The following catches integer(0), resutling from a call to which without a match
  stopifnot(!length(distance.vector) == 0)
  # If in any case you need to ignore NAs, use na.last = NA as parameter in order().
  ordered.neighbors <- order(distance.vector)
  return(ordered.neighbors[1:k])
}

#############################################################################
# Function to create and print a nice heatmap for benchmarking visualization.
plot.benchmark.heatmap <- function(heatmap, ordering_x, ordering_y, offset_rightplot = 2.05, yaxis = c(0, 1), textinboxsize = 3, yaxis_rightplot = yaxis, plotdimx = 10, plotdimy = 10, prunemin = FALSE, colorscheme = hmcol, compare_dist1 = FALSE,  compare = FALSE, plotlegend = TRUE, plotylabel = TRUE,ylabelrightplot = NULL,  plotylabelrightplot = TRUE){
  require('scales')
  #Prepare for ggplotting.
  ydimorig <- dim(heatmap)
  y <- as.data.frame(melt(heatmap))
  stopifnot(is.data.frame(y))

  colnames(y) <- c("col1", "col2", "value")
  y[, "col1"] <- factor(x = y[, "col1"], levels = rev(ordering_y))
  y[, "col2"] <- factor(x = y[, "col2"], levels = ordering_x)
  test <- sparseby(y, y[, "col1"], function(x) mean(x$value, na.rm = TRUE))
  colnames(test) <- c("col1", "value")
  y <- merge(y, test, by = "col1")
  colnames(y)[length(colnames(y))] <- "mean_value"
  colnames(y)[which(colnames(y) == "value.x")] <- "value"
  y$"value" <- as.numeric(as.vector(y$"value"))
  means.y <- ggplot(y) +
    geom_point(aes(col1, mean_value), size = 0.5) + coord_flip() + ylim(yaxis_rightplot)
    geom_hline(yintercept = 0)
    scale_colour_gradientn(colours = colorscheme, limits=yaxis)

  if (plotylabelrightplot == TRUE){
    means.y <- means.y +  theme(axis.title.y = element_blank(),
                                   axis.text.y = element_blank(),
                                   axis.ticks.y = element_blank(),
                                   axis.ticks.x = element_blank(),
                                   legend.position="top",
                                   legend.title = element_text("AUC"),
                                   axis.title.x = element_text(size = 8),
                                axis.text.x = element_text(angle = 45, hjust = 1, size = 6.5),
                                   plot.margin = unit(c(0.18, 0.2, (offset_rightplot - 0.25), 0), "cm")) +ylab("mean AUC")
    
  } else {
    means.y <- means.y +  theme(axis.title.y = element_blank(),
                                axis.text.y = element_blank(),
                                axis.text.x = element_blank(),
                                axis.ticks.x = element_blank(),
                                axis.ticks.y = element_blank(),
                                legend.position="top",
                                legend.title = element_text("AUC"),
                                plot.margin = unit(c(0.18, 0.2, offset_rightplot, 0), "cm")) + ylab("")
    
  }
  if (length(ylabelrightplot) != 0){
    means.y <- means.y + ylab(ylabelrightplot)
  }
  p <- ggplot(y,  aes(col2, col1)) + geom_tile(aes(fill = value))
  # if (compare == TRUE) {
  #   p <- p + scale_fill_gradientn(colours = colorscheme, values = c(0, 0.1, 0.5, 0.9, 1), limits=yaxis)
  # } else if (compare_dist1 == TRUE){
  #   p <- p + scale_fill_gradientn(colours = colorscheme, values = c(0, 0.5, 0.99, 1), limits=yaxis)
  # } else {
  #   p <- p +  scale_fill_gradientn(colours = colorscheme, limits=yaxis)
  # }
  p <- p +  scale_fill_gradientn(colours = colorscheme, limits=yaxis)
  if (plotylabel == FALSE){
    p <- p + geom_text(aes(label=round(value,2)), size = textinboxsize) + theme(legend.position = "top",
                                                                      axis.title.x = element_blank(),
                                                                      axis.title.y = element_blank(),
                                                                      axis.text.x = element_blank(), 
                                                                      axis.text.y = element_text(size = 7.2))
    
    
  } else {
    p <- p + geom_text(aes(label=round(value,2)), size = textinboxsize) + theme(legend.position = "top",
                                                                      axis.title.x = element_blank(),
                                                                      axis.title.y = element_blank(),
                                                                      axis.text.x = element_text(angle = 45, hjust = 1, size = 7), 
                                                                      axis.text.y = element_text(size = 7.2))
    
    
  }
  
  
  tmp <- ggplot_gtable(ggplot_build(p))
  leg <- which(sapply(tmp$"grobs", function(x) x$"name") == "guide-box")
  mylegend <- tmp$"grobs"[[leg]]
  if (plotlegend == TRUE){
    p3.grob <- arrangeGrob(mylegend, arrangeGrob(p + theme(legend.position="none"),
                                                 means.y + theme(legend.position="none")  + xlab("mean across data"),
                                                 nrow=1, widths = c(7, 1.5)),
                           nrow=2, heights=c(0.5, 3.5))
  } else {
    p3.grob <- arrangeGrob(arrangeGrob(p + theme(legend.position="none"),
                                                 means.y + theme(legend.position="none")  + xlab("mean across data"),
                                                 nrow=1, widths = c(7, 1.5)),
                           nrow=1, heights=c(3.5))
  }
  return(p3.grob)
}


### Function to adjust for library size
normalize <- function(otu_table_input, method){
  stopifnot(!is.null(otu_table_input))
  stopifnot(!is.null(method))
  otu_table <- otu_table_input
  normalization.method <- method
  if (normalization.method == "tss") {
    message('performing tss')
    otu_table <- apply(otu_table, 2, function(x) x/sum(x))
  } else if (normalization.method == "rarefy"){
    message('rarefying')
    # rarefying depth is the smallest library size.
    phyloseq.object <- phyloseq(otu_table(otu_table, taxa_are_rows = TRUE))
    message("removing samples which have a library size smaller than 301.")
    remove_index <- which(apply(otu_table, 2, sum) < 1000) # Remove samples which have a sampling depth lower than 300 before feature filtering.
    if (length(remove_index) > 0){
      otu_table <- otu_table[, -remove_index]
    }
    message("Number of samples removed: ")
    message(length(remove_index))
    otu_table <- rarefy_even_depth(phyloseq.object, sample.size = min(apply(otu_table, 2, sum)), rngseed = 711)
    otu_table <- as.data.frame(otu_table(otu_table))
    stopifnot(all(apply(otu_table,2,sum) == min(apply(otu_table, 2, sum))))
  } else if (normalization.method == "none"){
    message("not normalizing")
  } else {
    write("Normalizationmethod method unknown; exiting", stderr())
    q()
  }
  
  return(otu_table)
  
}

# Function to transform data

transform_data <- function(otu_table_input, method){
  stopifnot(!is.null(otu_table_input))
  stopifnot(!is.null(method))
  otu_table <- otu_table_input
  transformationmethod <- method
  if (transformationmethod == "log") {
    # Pseudocount should be added in a data-dependent manner (as argued by Costea et al, 2014), where the order of magnitude of the pseudocount should be dictated by the smallest non-zero value of the dataset.
    # We thus add a pseudocount which is equal to the smallest count value, rounded down to the closest power-transformation of 10. (e.g. 0.2152 becomes 0.1; 212.3 becomes 100). Note that 1 becomes 0.1, 0.1 becomes 0.01 etc. through the substraction of a very small number.
    min.nz.value <- min(as.matrix(otu_table)[which(otu_table != 0)])
    rounded.down.min <- 10^floor(log10(min.nz.value-10^-12))
    otu_table <- log10(otu_table + rounded.down.min) 
  } else if (transformationmethod == "asinh"){
    otu_table <- asinh(otu_table)
  } else if (transformationmethod == "identity"){
    # This is just for code consistency
    otu_table <- otu_table
  } else if (transformationmethod == "rank") {
    # Transform abundances into ranked abundances, computingthe average when there are ties.
    otu_table <- apply(otu_table, 2, function(x) rank(x, ties.method = "average"))
  } else if (transformationmethod == "qualitative"){
    # Transform to presence/absence table
    ### For some reason, this only works on a matrix.
    orig_dim <- dim(otu_table)
    rnames <- rownames(otu_table)
    cnames <- colnames(otu_table)
    otu_table <- as.matrix(otu_table)
    otu_table[which(otu_table != 0)] <- 1
    otu_table <- as.data.frame(otu_table)
    rownames(otu_table) <- rnames
    colnames(otu_table) <- cnames
    # Small sanity check
    stopifnot(orig_dim == dim(otu_table))
    
    # The hellinger, chord and chi.square data transformations are ways to transform
    # Absolute read counts into values that make more or less sense in conjunction with the euclidean distance.
    
    # I have commented these out now. Fix the "otu_table_original" (not defined) once in case you ever use these.
    
  # } else if (transformationmethod == "hellinger"){
  #   # This distance is easier computable using the raw reads and the decostand function. In fact,
  #   # We need it for the chi-square distance (doesn't work with tss)
  #   otu_table <- decostand(otu_table_original, method = "hellinger", MARGIN = 2)
  # } else if (transformationmethod == "chord"){
  #   # This distance is easier computable using the raw reads and the decostand function. In fact,
  #   # We need it for the chi-square distance (doesn't work with tss)
  #   # I checked with some examples that even though the official definition of the chord distance is based on raw counts
  #   # Mathematically there's no difference between computing it on the raw or tss-transformed counts.
  #   otu_table <- decostand(otu_table_original, method = "normalize", MARGIN = 2)
  # } else if (transformationmethod == "chisq") {
  #   # This distance is easier computable using the raw reads and the decostand function. In fact,
  #   # We need it for the chi-square distance (doesn't work with tss)
  #   print("chisq...")
  #   otu_table <- decostand(otu_table_original, method = "chi.square", MARGIN = 1)
  } else {
    write("Transformation method unknown; exiting", stderr())
    q()
  }
  return(otu_table)
}

# Function to calculate distances

calc_distances <- function(otu_table_input, method){
  require(vegan)
  stopifnot(!is.null(otu_table_input))
  stopifnot(!is.null(method))
  otu_table <- otu_table_input
  distance <- method
  # Calculate distance
  if (is.null(distance)){
    warning("Not computing any distance measure since none parsed")  
  } else if (distance == "JSD"){
    distances <- dist.JSD_c(otu_table, workingDirectory = JSD.dir)
  } else if (distance == "correlation") {
    # distances <-1-abs(cor(otu_table, method='pearson'))  
    distances <-(1-cor(otu_table, method='pearson'))/2
  #} else if (distance == "spearman") {
  #  distances <-1-abs(cor(otu_table, method='spearman'))
  #} else if (distance == "spearmanAlt1") {
  #  distances <-(1-cor(otu_table, method='spearman'))/2
  #} else if (distance == "spearmanAlt2") {
  #  distances <-sqrt((1-(cor(otu_table, method='spearman'))^2))
  #} else if (distance == "spearmanexclDouble0"){
    # Preallocate memory with distance object
  #  distances <- matrix(NA, dim(otu_table)[2], dim(otu_table)[2])
    # Exclude double zeroes from correlation.
  #  for (column_it1 in 1:dim(otu_table)[2]){
  #    for (column_it2 in 1:dim(otu_table)[2]){
  #      excl.idx <- which(otu_table[,column_it1] == 0 & otu_table[,column_it2] == 0)
  #      distances[column_it1, column_it2] <-  1 - abs(cor(otu_table[,column_it1][-excl.idx], otu_table[,column_it2][-excl.idx], method = "spearman"))
  #    }
   # }
  #  rownames(distances) <- colnames(otu_table)
  #  colnames(distances) <- colnames(otu_table)
  #  distances <- as.dist(distances)
  } else if (distance == "kuczynskiGower"){
    distances <- as.dist(kuczynskiGower(otu_table))
  } else if (distance == "newgower"){
    distances <- as.dist(newgower(otu_table))
  } else if (distance == "weightedJaccard"){
    # See generalized jaccard index on wikipedia.
    # The performance of this distance is very, very bad. I'm pretty sure I implemented it properly.
    # Update: Apparently, this only makes sense for non-negative entries (At least wikipedia says its only defined for non-negative entries. Im not quite sure why this would be 
    # restrictive to only non-negative entries.)
    distances <- matrix(NA, dim(otu_table)[2], dim(otu_table)[2])
    for (column_it1 in 1:dim(otu_table)[2]){
      for (column_it2 in 1:dim(otu_table)[2]){
        distances[column_it1, column_it2] <- sum(apply(cbind(otu_table[,column_it1], otu_table[,column_it2]), 1, min)) / sum(apply(cbind(otu_table[,column_it1], otu_table[,column_it2]), 1, max))
      }
    }
    rownames(distances) <- colnames(otu_table)
    colnames(distances) <- colnames(otu_table)
    distances <- as.dist(distances)
  } else if (distance == "unifrac"){
    # Read tree
    tree <- read_tree(treedir)
    # Root tree
    set.seed(seed = 1337)
    print(tree)
    tree <- root(tree, sample(tree$tip.label, 1), resolve.root = TRUE)
    print(tree)
    # Build phyloseq object
    otu_table <- otu_table(otu_table, taxa_are_rows = TRUE)
    phyloseq_object <- phyloseq(otu_table, tree)
    # compute Unifrac distance
    # normalized: (Optional). Logical. Should the output be normalized such that values range
    # from 0 to 1 independent of branch length values? Default is TRUE. Note that
    # (unweighted) UniFrac is always normalized by total branch-length, and so this
    # value is ignored when weighted == FALSE. 
    distances <- UniFrac(phyloseq_object, weighted=FALSE)
  } else if (distance == "weightedUnifrac"){
    # Read tree
    tree <- read_tree(treedir)
    # Root tree
    set.seed(seed = 1337)
    print(tree)
    tree <- root(tree, sample(tree$tip.label, 1), resolve.root = TRUE)
    print(tree)
    # Build phyloseq object
    otu_table <- otu_table(otu_table, taxa_are_rows = TRUE)
    phyloseq_object <- phyloseq(otu_table, tree)
    # normalized: (Optional). Logical. Should the output be normalized such that values range
    #from 0 to 1 independent of branch length values? Default is TRUE. Note that
    #(unweighted) UniFrac is always normalized by total branch-length, and so this
    #value is ignored when weighted == FALSE. 
    distances <- UniFrac(phyloseq_object, weighted=TRUE)
  } else {
    # Transpose since vegdist assumes samples in rows.
    otu_table <- t(otu_table)
    distances <- vegdist(x = otu_table, method = distance)
  }
  return(distances)
}

# Calculate kNN-based AUC values. For each class, take that class as the positive class.
#   for each sample in the dataset, determine the distances to the k nearest samples of this (positive) class as well as the distances to the k nearest sample of any other class.
#   Sum those two sets of distances up and compute the ratio of it. The vector of these ratios is the prediction score used in this algorithm.
do.knn.auc.benchmark <- function (distance_input, label_input, approach = "weighted", illustrate_index = NULL,  k){
  suppressMessages(require(pROC))
  suppressMessages(require("flexclust"))
  distance <- distance_input
  label <- label_input
  num.runs <- length(label)
  # Set k to a value which is somewhat below the k used in kNN. Something like floor(sqrt(N) * (2/3)), unless your smallest class size is smaller, 
  # in which case you should set it to this size.
  # This check is done in evaluate_distance.r
  outer.k <- k
  nonflipped.AUC <- list()
  for (class.idx in 1:length(unique(label))){
    curr.class <- unique(label)[class.idx]
    # Preallocate results
    ratios_of_sums <- list()
    # for EVERY sample, do...
    for (sample in 1:dim(distance)[1]){
      # Truncate distance matrix to only members of the positive / negative class. Include current sample of interest.
      # Make sure to exclude the current sample from the respective distance matrix.
      # See if the current index is in pos.class.all or neg.class.all and remove it if that is the case.
      # # is our sample of interest part of pos.class.all?
      pos.class.all <- which(label == curr.class)
      neg.class.all <- which(label != curr.class)
      posclass.distances <- distance[sample, pos.class.all]
      negclass.distances <- distance[sample, neg.class.all]
      if (!is.na(match(sample, pos.class.all))){
        #print(paste("sample ", sample, " is in positive class ", curr.class, sep = ""))
        posclass.distances <- posclass.distances[- which(pos.class.all == sample)]
      } else if (!is.na(match(sample, neg.class.all))){
        #print(paste("sample ", sample, " is in negative class, which are all classes except ", curr.class, sep = ""))
        negclass.distances <- negclass.distances[- which(neg.class.all == sample)]
      } else{
        stop("Something is wrong.")
      }
      # Create distance matrices.
      #posclass.distances <- distance[c(sample, pos.class.all), c(sample, pos.class.all)]
      #negclass.distances <- distance[c(sample, neg.class.all), c(sample, neg.class.all)]
      # our sample of interest is not at position 1 of posclass.distances and negclass.distances, respectively.
      # This step could potentially be made much for efficient by not creating posclass.distances and negclass.distances
      # as separate objects. Instead, one could rewrite k.nearest.neighbors to only take a vector and not a whole distance matrix
      # and just pass the vector of positive and negative neighbors. Let's see how slow the code really is.
      index.nearest.neighbors.toposclass <- k.nearest.neighbors(posclass.distances, k = outer.k)
      distances.to.closest.samples.posclass <- posclass.distances[index.nearest.neighbors.toposclass]
      # Do the same now for interclass distances
      index.nearest.neighbors.tonegclass <- k.nearest.neighbors(negclass.distances, k = outer.k)
      distances.to.closest.samples.negclass <- negclass.distances[index.nearest.neighbors.tonegclass]
      # Calculate the ratio of sums. We want high values for "good" results.

      if (approach == "weighted"){
        # The weighted approach simply takes the mean of the distances and computes the ratio
        # Note that mean and sum yields the same result as the number of distances to positive and negative samples
        # is identical.
        ratio_of_sums <- mean(as.numeric(distances.to.closest.samples.negclass)) / mean(as.numeric(distances.to.closest.samples.posclass))
      } else if (approach == "rank") {
        # Compute ranks before summing
        all_distances <- c(distances.to.closest.samples.negclass, distances.to.closest.samples.posclass)
        all_distances <- unlist(all_distances)
        all_distances <- rank(x = all_distances, ties.method = "average")
        distances.to.closest.samples.negclass <- all_distances[1:length(distances.to.closest.samples.negclass)]
        distances.to.closest.samples.posclass <- all_distances[(length(distances.to.closest.samples.posclass) + 1): length(all_distances)]
        ratio_of_sums <- sum(distances.to.closest.samples.negclass) / sum(distances.to.closest.samples.posclass)
      } else {
        message("approach unknown, exiting")
        q()
      }
      # This if condition is just for illustrative purposes and debugging.
      if (identical(illustrate_index, sample)){
        message("Current positive class")
        print(curr.class)
        message("Current sample of interest")
        print(sample)
        message("indices of closest samples of positive class")
        print(match(rownames(posclass.distances)[index.nearest.neighbors.toposclass], rownames(distance)))
        message("indices of cloest samples of not-positive class")
        print(match(rownames(negclass.distances)[index.nearest.neighbors.tonegclass], rownames(distance)))
        message("distances to closest samples of positive class")
        print(as.numeric(distances.to.closest.samples.posclass))
        message("distances to closest samples of negative class")
        print(as.numeric(distances.to.closest.samples.negclass))
        print(ratio_of_sums)
      }
      ratios_of_sums[[sample]] <- ratio_of_sums
    }
    # Generate vectors for AUC analysis.
    #print(class.idx)
    roc.vector <- unlist(ratios_of_sums)
    roc.labels <- as.vector(label)
    #print(roc.labels)
    roc.labels <- replace(roc.labels, which(label == curr.class), 1)
    roc.labels <- replace(roc.labels, which(label != curr.class), 0)
    # Make sure that 1 is understood as the control class label
    # Note that the order of levels of the following factor is not the same as for the other AUC-based benchmarks.
    # I have decided to stick with this "reversed" factoring the evaluation itself is more intuitive when samples of the current positive class
    # Have a small ratio of distances to the negative class / distance to the positive class. Look at how I calculate ratio_of_sums in order to understand.
    # In case this is confusing for a future developer / user, simply swap the nominator and denominator of ratio_of_sums as well as the levels argument of the line below and you will get the same result.
    roc.labels <- factor(as.numeric(roc.labels), levels = c(1,0))

    # Using the roc function from the pROC package, specifying the direction properly (in order to allow for negative roc curves)
    # Here, "control samples" (The ratios for samples of the current positive class) have a lower value than "case" samples, thus put direction = ">". Look into pROC documentation for more elaborate explanation.
    nonflipped.AUC[[class.idx]] <- pROC::roc(roc.labels, roc.vector, direction = ">", ci = FALSE, algorithm = 2)
    names(nonflipped.AUC)[class.idx] <- as.vector(curr.class)
  }
  return(nonflipped.AUC)
}